import 'package:flutter/material.dart';
import 'package:usefood/pages/home.dart';
import 'package:usefood/pages/inventory.dart';
import 'package:usefood/pages/scan.dart';
import 'package:usefood/pages/settings.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _currentIndex = 0;
  PageController _pageController = PageController(initialPage: 0);

  final _bottomNavigationBarItems = [
    BottomNavigationBarItem(
        backgroundColor: Colors.green,
        icon: Icon(Icons.camera_alt_outlined, color: Colors.white),
        label: 'Scanner'),
    BottomNavigationBarItem(
        backgroundColor: Colors.green,
        icon: Icon(Icons.inventory_2_outlined, color: Colors.white),
        label: 'Inventaire'),
    BottomNavigationBarItem(
        backgroundColor: Colors.green,
        icon: Icon(Icons.home_max_outlined, color: Colors.white),
        label: 'Dashboard'),
    BottomNavigationBarItem(
        backgroundColor: Colors.green,
        icon: Icon(Icons.settings_applications_outlined, color: Colors.white),
        label: 'Réglages'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        onPageChanged: (newIndex) {
          setState(() {
            _currentIndex = newIndex;
          });
        },
        children: [
          ScanPage(),
          InventoryPage(),
          HomePage(),
          SettingsPage(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        items: _bottomNavigationBarItems,
        selectedLabelStyle: TextStyle(color: Colors.white),
        showUnselectedLabels: false,
        onTap: (index) {
          _pageController.animateToPage(index,
              duration: Duration(milliseconds: 500), curve: Curves.ease);
        },
      ),
    );
  }
}
