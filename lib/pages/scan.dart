import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class ScanPage extends StatefulWidget {
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  String? scanResult;

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              ElevatedButton.icon(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.amber, onPrimary: Colors.black),
                  icon: Icon(Icons.camera_alt_outlined),
                  label: Text('Scanner un aliment'),
                  onPressed: scanBarCode),
              SizedBox(height: 20),
              Text(
                  scanResult == ""
                      ? 'Scannez votre aliment !'
                      : 'Résultat du scan : $scanResult',
                  style: TextStyle(fontSize: 18)),
            ],
          ),
        ),
      );

  Future scanBarCode() async {
    String scanResult;
    scanResult = await FlutterBarcodeScanner.scanBarcode(
        "#ff6666", "Annuler", true, ScanMode.BARCODE);
    if (!mounted) return;
    setState(() => this.scanResult = scanResult);
  }
}
